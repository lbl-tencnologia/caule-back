require_relative 'boot'

map('/users') { run UsersController }
map('/plant') { run PlantsController }
map('/measurement') { run MeasurementsController }