Running app:
`rackup`

Endpoints:
```http://localhost:9292/users
http://localhost:9292/plant
http://localhost:9292/measurement
```
