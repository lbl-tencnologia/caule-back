require 'data_mapper'
require 'json'
require 'require_all'
require 'sinatra/base'

require_all 'app/**/*.rb'