class UsersController < Sinatra::Base
  before do
    next if request.get? || request.delete?
    request.body.rewind
    @request_payload = JSON.parse(request.body.read)
  end

  after do
    content_type 'application/json'
  end

  post '/' do
    user = User.parse(@request_payload)
    user.to_json
  end

  delete '/' do
    {deleted: 1}.to_json
  end

  put '/' do
    user = User.parse(@request_payload)
    user.to_json
  end

  get '/' do
    user = User.parse({
      name: 'Mocked User',
      email: 'mockeduser@gmail.com',
      password: 'password',
      birthday: '2018-11-23'
    })
    user.to_json
  end
end