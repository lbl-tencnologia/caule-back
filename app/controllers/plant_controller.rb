class PlantsController < Sinatra::Base
  before do
    next if request.get? || request.delete?
    request.body.rewind
    @request_payload = JSON.parse(request.body.read)
  end

  after do
    content_type 'application/json'
  end

  post '/' do
    plant = Plant.parse(@request_payload)
    plant.to_json
  end

  delete '/' do
    {deleted: 1}.to_json
  end

  put '/' do
    plant = Plant.parse(@request_payload)
    plant.to_json
  end

  get '/' do
    plant = Plant.parse({
      user_id: 1,
      name: 'Bromélia',
      place_name: 'Casa',
      type: 'Plant Type'
    })
    plant.to_json
  end
end