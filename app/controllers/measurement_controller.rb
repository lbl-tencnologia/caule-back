class MeasurementsController < Sinatra::Base
  before do
    next if request.get? || request.delete?
    request.body.rewind
    @request_payload = JSON.parse(request.body.read)
  end

  after do
    content_type 'application/json'
  end

  post '/' do
    measurement = Measurement.parse(@request_payload)
    measurement.to_json
  end

  delete '/' do
    {deleted: 1}.to_json
  end

  put '/' do
    measurement = Measurement.parse(@request_payload)
    measurement.to_json
  end

  get '/' do
    measurement = Measurement.parse({
      plant_id: 1,
      type: 'Temperatura',
      measurement: 27.5,
      iot_number: 1
    })
    measurement.to_json
  end
end