class BaseModel
  def self.parse(params)
    params.each do |key, value|
      instance_variable_set("@#{key}", value)
    end
  end

  def self.to_json
    hash = Hash[
      instance_variables.map do |name|
        [name, instance_variable_get[name]]
      end
    ]
    hash.to_json
  end
end